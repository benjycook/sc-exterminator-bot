package javabot;

public interface Manager {
	
	public Command getCurrentCommand();
	public Request getCurrentRequest();
	public boolean doCommand(Command cmd);
}
