package javabot;


import java.util.ArrayList;
import java.util.Map;

import javabot.model.Unit;
import javabot.types.UnitType.UnitTypes;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class Commander {
	
	private Manager builder;
	private Manager Scout;
	private Manager Marshal;
	private ArrayList<Manager> managers;
	private Map<String,Request> requests;
	public static JNIBWAPI bwapi;
	
	public Commander() {
		
	}
	
	public static void set_bwapi(JNIBWAPI bwapi) {
		Commander.bwapi = bwapi;
	}
	
	public void first_tick() {
		bwapi.printText("Commander first tick");
		//builder = new Builder(this);
		
		

		//managers.add(Builder);
		Request req = new Request("Train scv") {
			
			
			
			public void callback() {
				
				
				
			}
			
			public boolean checkPredicate() {
				return Commander.bwapi.getSelf().getMinerals() >= 50 ;
			}
		};
		
		for (Unit unit : Commander.bwapi.getMyUnits()) {
			// if this unit is a command center (Terran_Command_Center)
			if (unit.getTypeID() == UnitTypes.Terran_Command_Center.ordinal()) {
				// if it's training queue is empty
				Commander.bwapi.train(unit.getID(), UnitTypes.Terran_SCV.ordinal());
				return;
			}
		}
//		requests.put(req.id, req);
//		bwapi.printText("Commander has "+requests.size()+" requests");
//		for(Request request : requests.values()) {
//			bwapi.printText("checking request "+request);
//			if(request.checkPredicate()) {
//				bwapi.printText("calling back "+request);
//				request.callback();
//			}
//			
//		}

	}
	
	public void tick() {
		// process any incoming requests and add them to the requests array
		//bwapi.printText("Commander tick start");
		/*for (Manager manager : managers) {
			requests.add(manager.getCurrentRequest());
		}*/
		
		
		// Prioritize them based on round robin of giver
		
		
		// Issue command to relevant manager
		
	}
}
