package javabot;

import java.awt.Point;
import java.util.Map;

import javabot.model.Unit;

public class Squad {

	private Map<Integer, Unit> squad;
	Point currentAttackPoint;
	Point currentCenterPoint;
	
	public int add(Unit u) {
		squad.put(u.getID(), u);
		return squad.size();
	}
	
	private void regroup() {
		
	}
	
	public int size() {
		return squad.size();
	}
	
	public void attack(Point pt) {
		currentAttackPoint = pt;
		regroup();
		for(Unit u : squad.values()) {
			Commander.bwapi.attack(u.getID(), pt.x, pt.y);
		}
	}
	
	public void update() {
		currentCenterPoint.
	}
	
}
