package javabot;

public abstract class Request {

	public String id;
	public abstract void callback();
	public abstract boolean checkPredicate();
	
	public Request(String id) {
		this.id = id;
	};
	
	public String toString() {
		return id;
	};
}
